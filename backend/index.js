const express = require('express')
const cors = require('cors')
const app = express()
const port = process.env.NODE_PORT

const corsOptions = {
    origin: process.env.CORS,
    credentials: true,
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions));

app.get('/', (req, res) => {
    res.send('Hello World!')
})

const http = require('http').createServer().listen(process.env.SOCKET_IO_PORT, '0.0.0.0');
const io = require('socket.io').listen(http);

let sequenceNumberByClient = new Map();

// https://gist.github.com/luciopaiva/e6f60bd6e156714f0c5505c2be8e06d8
io.on('connection', (socket) => {
    console.info(`Client connected [id=${socket.id}]`);
    // initialize this client's sequence number
    sequenceNumberByClient.set(socket, 1);

    // when socket disconnects, remove it from the list:
    socket.on("disconnect", () => {
        sequenceNumberByClient.delete(socket);
        console.info(`Client gone [id=${socket.id}]`);
    });
});

// sends each client its current sequence number
setInterval(() => {
    for (const [client, sequenceNumber] of sequenceNumberByClient.entries()) {
        client.emit("seq-num", sequenceNumber);
        sequenceNumberByClient.set(client, sequenceNumber + 1);
    }
}, 5000);


app.listen(port, () => {
  console.log(`Example app listening at ${port}`)
})